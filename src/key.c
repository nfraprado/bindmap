#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <ncurses.h>
#include <ctype.h>
#include "key.h"
#include "bindmap.h"

//width of keys, distance from one border to the other (one less than the
//apparent width)
static int keyw = 8;
//height of keys
static int keyh = 4;

//terminal color to be used for each combination of modifiers on a key
static int color[8] = {0, 1, 2, 3, 4, 5, 6, 7};

/*
 * Create a binding on a key
 * key      - key to have a binding added to
 * mod      - modifier of binding on the key
 * command  - command to be assigned to the binding
 */
int make_binding(struct key *key, char mod, char *command)
{
    static int bind_count = 0;
    //check if this command isn't already present in another binding
    int num = get_bind_num(command, bind_count);
    int bind_num = num >= 0 ? num : bind_count;
    //if the binding limit is reached, bail (shouldn't happen)
    if(bind_num > MAX_BINDINGS)
        return bind_num;

    //create the new binding and assign to the right modifier position on the key
    switch(mod){
        case MOD_NORMAL:
            key->normal_num = bind_num;
            key->normal = strdup(command);
            bindings[bind_num] = key->normal;
            break;
        case MOD_SHIFT:
            key->shift_num = bind_num;
            key->shift = strdup(command);
            bindings[bind_num] = key->shift;
            break;
            break;
        case MOD_CONTROL:
            key->control_num = bind_num;
            key->control = strdup(command);
            bindings[bind_num] = key->control;
            break;
    }
    if(num < 0)
        bind_count++;

    return bind_count;
}

/*
 * Create a new key
 * name     - name of the key
 * alt_name - alternative name of the key
 */
struct key *create_key(char *name, char *alt_name)
{
    struct key *key = (struct key*) malloc(sizeof(struct key));
    key->name = strdup(name);
    if (*alt_name == '\0')
        key->alt_name = NULL;
    else
        key->alt_name = strdup(alt_name);
    key->next = NULL;
    key->normal = NULL;
    key->shift = NULL;
    key->control = NULL;
    key->normal_num = -1;
    key->shift_num = -1;
    key->control_num = -1;
    return key;
}

/*
 * Free the memory of a key
 * key  - key to be destroyed
 */
void destroy_key(struct key *key)
{
    if(key->next != NULL)
        destroy_key(key->next);
    if(key->alt_name != NULL)
        free(key->alt_name);
    free(key->name);
    free(key->normal);
    free(key->shift);
    free(key->control);
    free(key);
}

/*
 * Get the color of a key based on the bindings assigned to it
 * key - key to check the color for
 */
char get_key_color(struct key *key)
{
    int color_index = 0;
    //Normal binding controls bit 0
    if (key->normal != NULL)
        color_index |= 0b1;
    //Shift binding controls bit 1
    if (key->shift != NULL)
        color_index |= 0b10;
    //Control binding controls bit 2
    if (key->control != NULL)
        color_index |= 0b100;
    //Returns +1 to map into ncurses color_pairs, which go from 1 to 8
    return color[color_index]+1;
}

/*
 * Draw a key on the screen
 * key      - key to be drawn
 * i        - number of the key in the current row
 * j        - number of the current row
 * sel_bind - selected binding
 */
void draw_key(struct key *key, int i, int j, int sel_bind)
{
    //draw key border
    mvvline(keyh*j+1, keyw*i, '|', keyh-1);
    mvvline(keyh*j+1, keyw*i+keyw, '|', keyh-1);
    mvhline(keyh*j, keyw*i, '-', keyw+1);
    mvhline(keyh*j+keyh, keyw*i, '-', keyw+1);

    //set key color pair
    attron(COLOR_PAIR(get_key_color(key)));

    //draw key background color
    for(int k = 0; k < keyh-1; k++)
        mvhline(keyh*j+1+k, keyw*i+1, ' ', keyw-1);

    //draw key name in bold
    attron(A_BOLD);
    mvprintw(keyh*j+1,keyw*i+1,"%3s", key->name);
    attroff(A_BOLD);

    //draw key normal binding number
    if (key->normal_num >= 0){
        if (key->normal_num == sel_bind){
            attron(COLOR_PAIR(color[0]+1));
            mvprintw(keyh*j+1,keyw*i+keyw-3,"%3d", key->normal_num);
            attron(COLOR_PAIR(get_key_color(key)));
        }
        else
            mvprintw(keyh*j+1,keyw*i+keyw-3,"%3d", key->normal_num);
    }
    else
        mvprintw(keyh*j+1,keyw*i+keyw-3,"   ");

    //draw key shift binding number
    if (key->shift_num >= 0)
        if (key->shift_num == sel_bind){
            attron(COLOR_PAIR(color[0]+1));
            mvprintw(keyh*j+keyh-1,keyw*i+1,"%3d", key->shift_num);
            attron(COLOR_PAIR(get_key_color(key)));
        }
        else
            mvprintw(keyh*j+keyh-1,keyw*i+1,"%3d", key->shift_num);
    else
        mvprintw(keyh*j+keyh-1,keyw*i+1,"   ");

    //draw key control binding number
    if (key->control_num >= 0)
        if (key->control_num == sel_bind){
            attron(COLOR_PAIR(color[0]+1));
            mvprintw(keyh*j+keyh-1,keyw*i+keyw-3,"%3d", key->control_num);
            attron(COLOR_PAIR(get_key_color(key)));
        }
        else
            mvprintw(keyh*j+keyh-1,keyw*i+keyw-3,"%3d", key->control_num);
    else
        mvprintw(keyh*j+keyh-1,keyw*i+keyw-3,"   ");

    //unset key color pair
    attroff(COLOR_PAIR(get_key_color(key)));
}

/*
 * Get the width of keys
 */
int get_key_width()
{
    return keyw;
}

/*
 * Get the height of keys
 */
int get_key_height()
{
    return keyh;
}

/*
 * Set the height of keys
 * height   - value to be set to
 */
void set_key_height(int height)
{
    keyh = clamp(height, 0, 3) + 3;
}

/*
 * Set the width of keys
 * width   - value to be set to
 */
void set_key_width(int width)
{
    keyw = clamp(width, 0, 3) + 7;
}

/*
 * Set the color mapping of the keys
 * color_s  - string of color values. Contains 8 digits from 0 to 7, the first
 *            corresponds to the color for keys with the first combination of
 *            modifiers and so on
 */
void set_key_colors(char *color_s)
{
    int len = strlen(color_s);
    if(len != 8){
        fprintf(stderr, "Invalid color string: must contain 8 digits\n");
        exit(1);
    }
    for(int i = 0; i < len; i++){
        if(isdigit(color_s[i]) && color_s[i] - '0' < 8)
            color[i] = color_s[i] - '0';
        else{
            fprintf(stderr, "Invalid color string: must consist of digits from 0 to 7\n");
            exit(1);
        }
    }
}
