#include <stdlib.h>
#include <string.h>
#include <ncurses.h>
#include "bindmap.h"
#include "keyboard.h"

/*
 * Create a keyboard object
 * keymap_filename  - keymap filename to provide the key layout
 * Returns a pointer to the keyboard
 */
struct row *create_keyboard(char *keymap_filename)
{
    struct row *keyboard = (struct row*) malloc(sizeof(struct row));
    keyboard->next = NULL;
    keyboard->first_key = NULL;
    parse_keys(keyboard, keymap_filename);

    return keyboard;
}

/*
 * Free the memory of a keyboard object
 * keyboard - the object to be freed
 */
void destroy_keyboard(struct row *keyboard)
{
    destroy_row(keyboard);
}

/*
 * Draw the keyboard on the screen
 * keyboard - keyboard to be drawn
 * sel_bind - selected binding
 */
void draw_keyboard(struct row *keyboard, int sel_bind)
{
    int i = 0, j = 0;
    int rows, cols;
    getmaxyx(stdscr, cols, rows);
    //iterate through the rows of the keyboard
    while(keyboard != NULL){
        if(get_key_height()*(j+1) > cols)
            break;

        i = 0;
        struct key *key = keyboard->first_key;
        //iterate through the keys of the keyboard
        while(key != NULL){
            //key out of screen bounds, dont draw
            if(get_key_width()*(i+1) > rows)
                break;
            draw_key(key, i, j, sel_bind);
            i++;
            key = key->next;
        }
        j++;
        keyboard = keyboard->next;
    }
}

/*
 * Add binding to the keyboard
 * keyboard - keyboard to have a binding added to
 * name     - name of the key
 * mod      - modifier of the binding
 * command  - command associated with the binding
 * Returns the total binding count
 */
int add_binding(struct row *keyboard, char *name, enum mod mod, char *command)
{
    struct key *key = find_key(keyboard, name);
    if(key)
        return make_binding(key, mod, command);
    else
        fprintf(stderr, "Error while binding key '%s': Key not found\n", name);
    return 0;
}

/*
 * Add a row to the keyboard
 * keyboard - keyboard to have a row added to
 */
void add_row(struct row *keyboard)
{
    struct row *row = keyboard;
    while(row->next != NULL)
        row = row->next;

    row->next = (struct row*) malloc(sizeof(struct row));
    row->next->next = NULL;
    row->next->first_key = NULL;
}


/*
 * Remove and free resources of a row
 * row  - row to be destroyed
 */
void destroy_row(struct row *row)
{
    if(row->next != NULL)
        destroy_row(row->next);
    if(row->first_key != NULL)
        destroy_key(row->first_key);
    free(row);
}

/*
 * Get the number of rows in a keyboard
 * keyboard - keyboard to be searched
 */
int get_keyboard_length(struct row *keyboard)
{
    struct row *row = keyboard;

    int j = 1;
    while(row->next != NULL){
        row = row->next;
        j++;
    }
    return j;
}

/*
 * Add a key to a keyboard
 * keyboard - keyboard to have a key added to
 * name     - name of the key, which will be displayed on the keyboard
 * alt_name - (optional) alternative name of the key. Used when the name to be
 *            displayed on the keyboard is different from the one used on the
 *            binding list
 */
void add_key(struct row *keyboard, char *name, char *alt_name)
{
    struct row *row = keyboard;
    while(row->next != NULL)
        row = row->next;

    if(row->first_key == NULL)
        row->first_key = create_key(name, alt_name);
    else{
        struct key *key = row->first_key;
        while(key->next != NULL)
            key = key->next;
        key->next = create_key(name, alt_name);
    }
}

/*
 * Find a key by name on the keyboard
 * keyboard - keyboard to be searched
 * name     - name of the key to be searched for
 */
struct key *find_key(struct row *keyboard, char *name)
{
    struct row *row = keyboard;

    while(row != NULL){
        struct key *key = row->first_key;
        while(key != NULL){
            if(strcasecmp(key->name, name) == 0 || (key->alt_name != NULL && strcasecmp(key->alt_name, name) == 0))
                return key;
            key = key->next;
        }
        row = row->next;
    }
    return NULL;
}
