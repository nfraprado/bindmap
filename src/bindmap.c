#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <ncurses.h>
#include <getopt.h>

#include "key.h"
#include "keyboard.h"
#include "bindmap.h"

#define max(a,b) \
  ({ __typeof__ (a) _a = (a); \
      __typeof__ (b) _b = (b); \
    _a > _b ? _a : _b; })
#define min(a,b) \
  ({ __typeof__ (a) _a = (a); \
      __typeof__ (b) _b = (b); \
    _a < _b ? _a : _b; })

#define MAXINPUT 3

//each position points to the command of a binding
char *bindings[MAX_BINDINGS] = { 0 };

/*
 * Parse the keys from a keymap file
 * keyboard     - the keyboard to receive the keys
 * keymap_file  - the name of the keymap file
 */
void parse_keys(struct row *keyboard, char *keymap_file)
{
    char *line = NULL;
    size_t len = 0;
    ssize_t read = 0;
    FILE *fptr;
    char *key_name = (char*) malloc(4*sizeof(char));
    char *alt_name = (char*) malloc(21*sizeof(char));
    *key_name = '\0';
    *alt_name = '\0';

    fptr = fopen(keymap_file, "r");
    if (fptr == NULL){
        fprintf(stderr, "Error while parsing keymap file: Unable to open file '%s'.\n", keymap_file);
        free_resources(keyboard);
        exit(1);
    }

    while ((read = getline(&line, &len, fptr)) != -1) {
        //blank line, add a row to the keyboard
        if(strcmp(line, "\n") == 0)
            add_row(keyboard);
        //tripple dash, add a blank key
        else if (strncmp(line, "---", 3) == 0){
            strcpy(key_name, " ");
            add_key(keyboard, key_name, alt_name);
        }
        //add a normal key
        else{
            sscanf(line, "%3s %20s", key_name, alt_name);
            add_key(keyboard, key_name, alt_name);
        }
        *alt_name = '\0';
    }
    fclose(fptr);
    free(line);
    free(key_name);
    free(alt_name);
}

/*
 * Parse the bindings for each key
 * keyboard     - the keyboard to receive the bindings
 * Returns the total unique bindings created
 */
int parse_bindings(struct row *keyboard)
{
    char *line = NULL;
    size_t len = 0;
    ssize_t read = 0;
    int bind_count = 0;

    //iterate through each line of the input
    while ((read = getline(&line, &len, stdin)) != -1) {
        char *sep = strchr(line, ' ');
        //malformed line
        if(sep == NULL)
            continue;
        //divide the string in two parts, the binding and the command
        char *cmd = sep+1;
        *sep = '\0';
        cmd[strlen(cmd)-1] = '\0';

        //add the binding to the key with the given modifier
        if(strstr(line, "Shift") != NULL)
            bind_count = add_binding(keyboard, strchr(line, '+')+1, MOD_SHIFT, cmd);
        else if(strstr(line, "Control") != NULL)
            bind_count = add_binding(keyboard, strchr(line, '+')+1, MOD_CONTROL, cmd);
        else
            bind_count = add_binding(keyboard, line, MOD_NORMAL, cmd);
    }
    free(line);
    return bind_count;
}

/*
 * Return the maximum length the pager can occupy
 */
int get_bind_pager_length(struct row *keyboard)
{
    int rows, cols;
    getmaxyx(stdscr, cols, rows);
    int kb_height = get_keyboard_length(keyboard)*get_key_height();
    int length = cols - (kb_height + 1);
    return length;
}

/*
 * Draw the binding list pager
 * keyboard     - the keyboard whose bindings will be drawn
 * blist_pos    - the first position of the bindings to be drawn
 * sel_bind     - selected binding
 */
void draw_binding_list(struct row *keyboard, int blist_pos, int sel_bind)
{
    int b = blist_pos;
    int l = 0;
    int rows = 0, cols = 0;
    getmaxyx(stdscr, rows, cols);
    int kb_height = get_keyboard_length(keyboard)*get_key_height();
    int length = rows - (kb_height + 1);

    /* Clear binding list to remove previous artifacts */
    move(kb_height+1, 0);
    clrtobot();

    while(bindings[b] && l < length){
        //draw the left-side binding on the pager
        if(b == sel_bind){
            attron(COLOR_PAIR(8));
            mvprintw(kb_height+1+l, 0, "%3d %s", b, bindings[b]);
            attroff(COLOR_PAIR(8));
        }
        else
            mvprintw(kb_height+1+l, 0, "%3d %s", b, bindings[b]);

        if(bindings[b+length]){
            //draw the right-side binding on the pager
            if(b+length == sel_bind){
                attron(COLOR_PAIR(8));
                mvprintw(kb_height+1+l, cols/2, "%3d %s", b+length, bindings[b+length]);
                attroff(COLOR_PAIR(8));
            }
            else
                mvprintw(kb_height+1+l, cols/2, "%3d %s", b+length, bindings[b+length]);
        }
        b++;
        l++;
    }
}

/*
 * Return the binding corresponding to a command
 * cmd          - the command string to be searched for
 * total_binds  - the total of bindings assigned
 */
int get_bind_num(char *cmd, int total_binds)
{
    for(int i = 0; i < total_binds; i++){
        if(strcmp(bindings[i], cmd) == 0)
            return i;
    }
    return -1;
}

/*
 * Clamp a value between other two
 * value    - value to be clamped
 * low      - lower bound
 * high     - upper bound
 * Returns the clamped value
 */
int clamp(int value, int low, int high)
{
    int i = min(value, high);
    i = max(low, i);

    return i;
}

/*
 * Get a valid position for the binding pager
 * keyboard     - keyboard being displayed
 * target_pos   - desired position for the pager
 * total_binds  - total of bindings assigned
 */
int get_valid_bind_pager_pos(struct row *keyboard, int target_pos, int total_binds)
{
    int offset = get_bind_pager_length(keyboard);

    return clamp(target_pos, 0, total_binds-2*offset);
}

/*
 * Initialize the ncurses interface
 */
void ncurses_init()
{
    /* Use newterm() instead of initscr() to be able to pipe the commands
     * through stdin without freezing the ncurses interface
     */
    FILE *f = fopen("/dev/tty", "r+");
    SCREEN *screen = newterm(NULL, f, f);
    set_term(screen);
    refresh();

    cbreak();
    keypad(stdscr, TRUE);
    noecho();
    curs_set(0);
    start_color();

    init_pair(1, COLOR_WHITE, COLOR_BLACK);
    init_pair(2, COLOR_WHITE, COLOR_RED);
    init_pair(3, COLOR_BLACK, COLOR_GREEN);
    init_pair(4, COLOR_BLACK, COLOR_YELLOW);
    init_pair(5, COLOR_WHITE, COLOR_BLUE);
    init_pair(6, COLOR_WHITE, COLOR_MAGENTA);
    init_pair(7, COLOR_BLACK, COLOR_CYAN);
    init_pair(8, COLOR_BLACK, COLOR_WHITE);
}

/*
 * Terminate the ncurses interface
 */
void ncurses_end()
{
    endwin();
}

/*
 * Free the resources before exiting
 * keyboard - keyboard to be freed
 */
void free_resources(struct row *keyboard)
{
    destroy_keyboard(keyboard);
    ncurses_end();
}

/*
 * Print the help message
 * prog_name     - the name used to invoke the program
 */
void print_help(char *prog_name)
{
    fprintf(stderr, "Usage: %s [options]\n\n", prog_name);
    fprintf(stderr, "  Options:\n"
            "    -k, --keymap filename      use 'filename' as keymap file\n"
            "    -w, --keyw width           set the width of a key as 'width' (from 0 to 3)\n"
            "    -i, --keyh height          set the height of a key as 'height' (from 0 to 3)\n"
            "    -o, --colors nnnnnnnn      set the color to use for each binding combination\n"
            "    -h, --help                 print this help message and exit\n");
}

/*
 * Get the binding to be selected from the input
 * init_num - Initial number. Considered as the first number from input
 */
int get_statbar_input_num(int init_num)
{
    char in_str[4] = "";

    int rows, cols;
    getmaxyx(stdscr, rows, cols);

    WINDOW *statbar = newwin(1, cols, rows-1, 0);
    curs_set(1);
    keypad(statbar, TRUE);

    mvwprintw(statbar, 0, 0, "%s", ":");
    int pos = 0;
    if(init_num >= 0){
        in_str[pos++] = init_num + '0';
        in_str[pos] = '\0';
    }

    mvwprintw(statbar, 0, 1, "%-3s", in_str);
    wmove(statbar, 0, pos+1);
    wrefresh(statbar);
    int ch;
    while((ch = wgetch(statbar)) != 27 && ch != '\n')
    {
        switch(ch)
        {
            case '0':
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9':
                if (pos < 3){
                    in_str[pos++] = ch;
                    in_str[pos] = '\0';
                }
                break;
            case KEY_BACKSPACE:
                pos = (pos > 0) ? pos - 1 : 0;
                in_str[pos] = '\0';
                break;
        }

        mvwprintw(statbar, 0, 1, "%-3s", in_str);
        wmove(statbar, 0, pos+1);
        wrefresh(statbar);
    }
    delwin(statbar);
    curs_set(0);
    if(ch == 27 || pos == 0)
        return -1;
    return atoi(in_str);
}

int main(int argc, char *argv[])
{
    struct row *keyboard = NULL;
    int blist_pos = 0;
    int total_binds = 0;
    char *keymap_filename = NULL;


    static struct option long_options[] = {
        {"keymap",  required_argument,  0,  'k'},
        {"keyw",    required_argument,  0,  'w'},
        {"keyh",    required_argument,  0,  'i'},
        {"colors",  required_argument,  0,  'o'},
        {"help",    no_argument,        0,  'h'},
        {0,         0,                  0,  0}
    };

    int option_index = 0;
    int opt;

    while((opt = getopt_long(argc, argv, "hk:c:w:i:o:", long_options, &option_index)) != -1){
        switch(opt){
            case 'k':
                keymap_filename = strdup(optarg);
                break;
            case 'w':
                set_key_width(atoi(optarg));
                break;
            case 'i':
                set_key_height(atoi(optarg));
                break;
            case 'o':
                set_key_colors(optarg);
                break;
            case 'h':
                print_help(argv[0]);
                exit(0);
                break;
            default:
                exit(1);
        }
    }

    //no keymap file passed as parameter
    if(keymap_filename == NULL){
        char *xdg_config_home = getenv("XDG_CONFIG_HOME");
        //get from .config
        if(xdg_config_home == NULL){
            char *home = getenv("HOME");
            keymap_filename = malloc((strlen(home)+24)*sizeof(char));
            sprintf(keymap_filename, "%s/.config/bindmap/keymap", home);
        }
        //get from XDG_CONFIG_HOME
        else{
            keymap_filename = malloc((strlen(xdg_config_home)+16)*sizeof(char));
            sprintf(keymap_filename, "%s/bindmap/keymap", xdg_config_home);
        }
    }

    keyboard = create_keyboard(keymap_filename);
    free(keymap_filename);
    total_binds = parse_bindings(keyboard);

    ncurses_init();

    int sel_bind = -1;

    draw_keyboard(keyboard, sel_bind);
    draw_binding_list(keyboard, blist_pos, sel_bind);
    refresh();

    int ch;
    //TUI loop. Exit with q or ESC
    while((ch = getch()) != 'q' && ch != 27){
        int offset = get_bind_pager_length(keyboard);
        switch(ch){
            //handle movement of the binding pager
            case 'j':
            case KEY_DOWN:
                blist_pos = get_valid_bind_pager_pos(keyboard, blist_pos+1, total_binds);
                break;
            case 'k':
            case KEY_UP:
                blist_pos = get_valid_bind_pager_pos(keyboard, blist_pos-1, total_binds);
                break;
            case 'l':
            case KEY_RIGHT:
                blist_pos = get_valid_bind_pager_pos(keyboard, blist_pos+offset, total_binds);
                break;
            case 'h':
            case KEY_LEFT:
                blist_pos = get_valid_bind_pager_pos(keyboard, blist_pos-offset, total_binds);
                break;
            case 'g':
                blist_pos = get_valid_bind_pager_pos(keyboard, 0, total_binds);
                break;
            case 'G':
                blist_pos = get_valid_bind_pager_pos(keyboard, total_binds-1, total_binds);
                break;
            //enter status bar number input mode
            case '0': case '1': case '2':
            case '3': case '4': case '5':
            case '6': case '7': case '8':
            case '9':
                sel_bind = get_statbar_input_num(ch-'0');
                //try to position selected binding on the first row in the second column
                blist_pos = get_valid_bind_pager_pos(keyboard, sel_bind-offset, total_binds);
                draw_keyboard(keyboard, sel_bind);
                break;
            case ':':
                sel_bind = get_statbar_input_num(-1);
                //try to position selected binding on the first row in the second column
                blist_pos = get_valid_bind_pager_pos(keyboard, sel_bind-offset, total_binds);
                draw_keyboard(keyboard, sel_bind);
                break;
            case KEY_RESIZE:
                //redraw everything when the screen gets resized
                clear();
                blist_pos = get_valid_bind_pager_pos(keyboard, blist_pos, total_binds);
                draw_keyboard(keyboard, sel_bind);
                break;
            default:
                sel_bind = -1;
                draw_keyboard(keyboard, sel_bind);
                break;
        }
        draw_binding_list(keyboard, blist_pos, sel_bind);
        refresh();
    }

    free_resources(keyboard);

    return 0;
}
