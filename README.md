# bindmap

## About

A binding visualizer for the i3/sway window manager.
It shows the command bindings for each key of the keyboard.

![Bindmap interface](bindmap.png?raw=true "Bindmap interface")

**Version 2.0 is a breaking change. The binding list should now be passed
through stdin instead of passing a command through -c**. See the **Initial
Setup** below for the new instructions.

## Installation

### ArchLinux

Just download and install the AUR package:
<https://aur.archlinux.org/packages/bindmap-git/>

### Manually

After downloading the folder, simply go inside of it and run:
```
make
sudo make install
```

## Initial Setup

**There are 2 manual steps needed for bindmap to work. If you skip them,
bindmap won't work:**

1. The first thing you need is to provide a keymap file, which defines the
	 layout of the keys in your keyboard. This file can be passed through the `-k`
	 parameter or by copying it to `~/.config/bindmap/keymap`. A sample keymap
	 file is provided in `example/keymap`. For more information on how to create
	 your keymap file see **Keymap file** below.

2. The other thing that is needed is to pipe the binding list through
	 ``bindmap``'s input. On i3, the following worked for me:

	 ```
	 i3-msg -t get_config | sed -n 's/^bindsym .*\$mod+//p' | bindmap
	 ```

   And on sway:

   ```
   swaymsg -t get_config | jq -r .config | sed -n 's/^bindsym .*\$mod+//p' | bindmap
   ```

	 If you have any problems with this, see **Binding list** below for more
	 details.

## Usage

At this point you should be all set and be greeted with ``bindmap``'s screen
showing all keys in your keyboard in the upper region and a pager with your
bindings in the lower region.

Each key on the keyboard is drawn as follows:
```
---------
|AAA NNN|
|       |
|SSS CCC|
---------
```

Where `AAA` is the name of the key, `NNN` the number of the normal binding,
`SSS` the number of the shift binding and `CCC` the number of the control
binding.
These numbers correspond to the numbers on the binding list.

Each key will be drawn with a color based on which bindings are set for it.

The binding list pager shows two columns of bindings, each one containing the
number of the binding and the command corresponding to it.

### Pager bindings

The binding list pager can be scrolled up or down by one binding using the `Up`
or `Down` arrow keys, respectively (or `k` and `j`).
The `Left` and `Right` keys scroll the pager up or down by half a page,
respectively (or `h` and `l`). `g` goes to the first entry and `G`, to the last.
Pressing `:` opens the prompt where a number with at most three digits can be
written. After pressing `Enter`, the pager will jump to the binding with that
number, and it will be highlighted in both the pager and the keyboard.
Pressing any digit will automatically open the prompt with that digit as
the first one.
Use `q` or `ESC` to exit.

### Parameters

All parameters are optional. Not passing them causes `bindmap` to use the
default values.

`-k, --keymap filename`: Use `filename` as keymap file.
By default, `bindmap` searches for a bindmap in
`$XDG_CONFIG_HOME/bindmap/keymap` and `~/.config/bindmap/keymap`.

`-w, --keyw width`: set the horizontal spacing between fields inside a key. The
value can be between 0 and 3.
The default is 1.

`-i, --keyh height`: set the vertical spacing between fields inside a key. The
value can be between 0 and 3.
The default is 1.

`-o, --colors nnnnnnnn`: set the color to use for each binding combination.
Each digit can be between 0 and 7, corresponding to colors black, red, green,
yellow, blue, magenta, cyan and white, respectively.
Each of the digits correspond to the color used when the following bindings are
present, from left to right:
no binding, normal, shift, normal and shift, control, control and normal,
control and shift, control and shift and normal.

`-h, --help`: display the help message and exit

### Keymap file

The keymap file is responsible for informing `bindmap` the layout of the
keyboard.

Each line consists of the name of the key (to be drawn on the keyboard), which
can be at most 3 characters long.
An optional alternative name can be given to the key by adding, after the key
name, a space and the alternative name, that is, `NAM alt_name`. This is useful
mainly for symbols, which have a different name in i3/sway's config. For
example:
```
- minus
```
This key will be drawn as a `-` inside bindmap, but will have the bindings
containing `minus` in the config.

An empty line indicates a new keyboard row.
If the name of the key is `---`, then it is drawn as an empty key. Useful for
spacing.

### Binding list

The binding list contains each of the bindings to be assigned to each key from
the keymap file.

Each line consists of the binding, which is a key symbol optionally prepended
with a '+' and a modifier (Shift or Control), followed by a space and then by
the command associated with the binding. Example:

```
f fullscreen toggle
Shift+f floating toggle
Control+f focus mode_toggle
s layout stacking
w layout tabbed
e layout toggle split
F1 exec firefox
```

*Note that the $mod+ is implicit*

To allow maximum flexibility, the binding list is read from the standard input
of the program, that is, it should be piped into ``bindmap``.

Since each binding list line is basically a bindsym line from i3/sway config,
it is very easy to write a command chain on the shell to read the config, parse
it into the appropriate format and pipe it into ``bindmap``, like the suggested
commands for i3 and sway in the **Initial Setup** section showed.

## Author

Nícolas F. R. A. Prado ([nfraprado](https://gitlab.com/nfraprado))
