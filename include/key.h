#ifndef KEY_H
#define KEY_H

/*
 * Key in the keyboard
 */
struct key{
    char *name; //Name of the key to be drawn on the keyboard
    char *alt_name; //Alternative name to match the bindings file
    char *normal; //Command of the normal binding
    char *shift; //Command of the shift binding
    char *control; //Command of the control binding
    int normal_num; //Number of the normal binding
    int shift_num; //Number of the shift binding
    int control_num; //Number of the control binding
    struct key *next; //Next key on the keyboard row
};

int make_binding(struct key *key, char mod, char *command);
struct key *create_key(char *name, char *alt_name);
void destroy_key(struct key *key);
char get_key_color(struct key *key);
void draw_key(struct key *key, int i, int j, int sel_bind);
int get_key_height();
int get_key_width();
void set_key_height(int height);
void set_key_width(int width);
void set_key_colors(char *color_s);

#endif
