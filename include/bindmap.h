#ifndef BINDMAP_H
#define BINDMAP_H

#include "keyboard.h"

#define MAX_BINDINGS 400

void parse_keys(struct row *keyboard, char *keymap_file);
int parse_bindings(struct row *keyboard);
int get_bind_pager_length(struct row *keyboard);
void draw_binding_list(struct row *keyboard, int blist_pos, int sel_bind);
int get_valid_bind_pager_pos(struct row *keyboard, int target_pos, int total_binds);
void free_resources(struct row *keyboard);
int get_bind_num(char *cmd, int max_binds);
int clamp(int value, int low, int high);

extern char *bindings[];

//possible modifiers for a binding. Normal means no modifier

#endif
