#ifndef KEYBOARD_H
#define KEYBOARD_H

#include "key.h"

struct row{
    struct key *first_key;
    struct row *next;
};

enum mod{
    MOD_NORMAL,
    MOD_SHIFT,
    MOD_CONTROL
};

struct row *create_keyboard(char *keymap_filename);
void destroy_keyboard(struct row *keyboard);
void draw_keyboard(struct row *keyboard, int sel_bind);
int add_binding(struct row *keyboard, char *name, enum mod mod, char *command);
void add_row(struct row *keyboard);
void destroy_row(struct row *row);
int get_keyboard_length(struct row *keyboard);
void add_key(struct row *keyboard, char *name, char *alt_name);
struct key *find_key(struct row *keyboard, char *name);

#endif
