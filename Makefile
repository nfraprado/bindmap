OUT = bindmap

SRCDIR = src
INCDIR = include

PREFIX ?= /usr/local

CC=gcc
CFLAGS = -Wall -g -Wextra -O2 -I$(INCDIR)

LIBS=-lncurses

SRC = $(wildcard $(SRCDIR)/*.c)

OBJ = $(patsubst %.c,%.o,$(SRC))

$(OUT): $(OBJ)
	$(CC) -o $@ $^ $(CFLAGS) $(LIBS)

$(OBJ): %.o: %.c
	$(CC) -o $@ -c $< $(CFLAGS)

install: $(OUT)
	install -D -m755 bindmap ${DESTDIR}${PREFIX}/bin/$(OUT)
	install -D -m644 example/keymap ${DESTDIR}${PREFIX}/share/bindmap/keymap

uninstall:
	@echo removing executable file from ${DESTDIR}${PREFIX}/bin
	rm -f ${DESTDIR}${PREFIX}/bin/$(OUT)

tags: $(SRC)
	ctags $(SRC) $(wildcard $(INCDIR)/*.h)

.PHONY: clean

clean:
	rm -f $(OBJ) $(OUT) tags
